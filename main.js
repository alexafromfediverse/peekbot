"use strict";

const { default: Mastodon } = require("megalodon");
const fetch = require("node-fetch");
const fs = require("fs");
const url = require("url");
const path = require("path");
const cproc = require("child_process");
const util = require("util");
const FormData = require("form-data");
const execFile = util.promisify(cproc.execFile);
const axios = require("axios");

const config = JSON.parse(fs.readFileSync("config.json"));

if(!fs.existsSync(config.tmp_image_path)) {
  fs.mkdirSync(config.tmp_image_path);
}

if(!fs.existsSync(config.converted_image_path)) {
  fs.mkdirSync(config.converted_image_path);
}

const waitForPipe = pipe => {
  return new Promise((resolve, reject) => {
    pipe.on("finish", () => resolve());
    pipe.on("error", (err) => reject(err));
  });
};

async function start() {
  const wss_client = new Mastodon(config.access_token, config.base_url_wss + "/api/v1");
  const client = new Mastodon(config.access_token, config.base_url + "/api/v1");

  const myinfo_res = await client.get("/accounts/verify_credentials", {});
  const myinfo = myinfo_res.data;

  console.log("me: " + myinfo.acct);

  const stream = wss_client.socket("/streaming", "user");

  stream.on("connect", () => {
    console.log("ready");
  });

  stream.on("notification", async (notification) => {
    if(notification.type === "mention") {
      const status = notification.status;
      console.log(status);
      let go = false;
      for(const tag of status.tags) {
        if(tag.name === "peekify") {
          go = true;
          break;
        }
      }
      if(go && status.media_attachments.length === 1) {
        const media = status.media_attachments[0];
        if(media.type === "image") {
          const parsed = url.parse(media.url);
          const basename = path.posix.basename(parsed.pathname);
          const res = await fetch(media.url);
          const destpath = path.join(config.tmp_image_path, basename);
          const dest = fs.createWriteStream(destpath);
          await waitForPipe(res.body.pipe(dest));
          const convpath = path.join(config.converted_image_path, basename);
          await execFile(config.peekify_executable, [ destpath, convpath ]);
          const imageData = fs.createReadStream(convpath);
          const formData = new FormData();
          formData.append("file", imageData, {
            filename: basename,
            contentType: res.headers.get("content-type"),
          });
          const attachment_res = await axios.post(config.base_url + "/api/v1/media", formData, {
            cancelToken: client.cancelTokenSource.token,
            headers: {
              Authorization: `Bearer ${client.accessToken}`,
              "Content-Type": `multipart/form-data; boundary=${formData.getBoundary()}`,
            },
          });
          const mentions = status.mentions.filter(m => m.acct !== myinfo.acct).map(m => "@" + m.acct).join(" ");
          const response = {
            in_reply_to_id: status.id,
            visibility: status.visibility,
            status: `${mentions} peekified for you x3`,
            media_ids: [
              attachment_res.data.id,
            ],
          };
          await client.post("/statuses", response);
        }
      }
    }
  });
}

start();
